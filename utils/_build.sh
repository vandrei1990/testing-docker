#!/bin/bash

set -x

ENV="$(dirname $0)/../.env"

# source the environment
# but exclude version if already in environment (externally set)
# e.g. when provided via pipeline variables
[ -z $BUILD_VERSION ] && . $ENV || { _BV=$BUILD_VERSION; . $ENV; BUILD_VERSION=$_BV; }

APPLICATION_NAME="${PWD##*/}"
: "${BUILD_VERSION:=latest}"
BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
TAG="${APPLICATION_NAME}:${BUILD_VERSION}"

# override image tag when running in Gitlab CI
[ -z "${CI_REGISTRY_IMAGE}" ] || TAG="${CI_REGISTRY_IMAGE}/${TAG}"

echo $TAG > $TAG_FILENAME

docker build \
  -t $TAG \
  --build-arg BUILD_DATE="$BUILD_DATE" \
  --build-arg APPLICATION_NAME="${APPLICATION_NAME}" \
  --build-arg BUILD_VERSION="${BUILD_VERSION}" \
  -f Dockerfile \
  .
