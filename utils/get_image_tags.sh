#!/bin/bash -x

# Build docker images for all containers - useful when building images locally

. "$(dirname $0)/../ztp/.env"

WORKDIR="$(dirname $0)/../ztp"
: ${TAG_FILENAME:=.tag}
EC=0

cd $WORKDIR

CONTAINERS=$(find . -maxdepth 1 -type d ! -path . | xargs -I P basename P)

for C in $CONTAINERS
do
  TAG="${C}/${TAG_FILENAME}"
  [ -s $TAG ] && cat $TAG
done

exit $EC
